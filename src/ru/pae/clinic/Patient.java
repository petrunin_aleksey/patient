package ru.pae.clinic;

public class Patient {
    private String surname;// фамилия пациента
    private int numberOfBorn;// год рождения пациента
    private int numberOfCard;// номер карточки пациента
    private boolean dis;// диспансеризация

    Patient(String surname, int numberOfBorn, int numberOfCard, boolean dis) {
        this.surname = surname;
        this.numberOfBorn = numberOfBorn;
        this.numberOfCard = numberOfCard;
        this.dis = dis;
    }
    public Patient() {
        this("не указана", 0, 0, false);
    }

    String getSurname() {
        return surname;
    }

    int getNumberOfBorn() {
        return numberOfBorn;
    }
    public int getNumberOfCard() {
        return numberOfCard;
    }

    boolean isDis() {
        return dis;
    }

    /**
     * Вывод информации о пациенте
     *
     * @return информация о пациенте
     */
    @Override
    public String toString() {
        return "Пациент{" +
                "Фамилия ='" + surname + '\'' +
                ", Год Рождения =" + numberOfBorn +
                ", Номер Карты =" + numberOfCard +
                ", Диспансеризация =" + dis +
                '}';
    }
}

