package ru.pae.clinic;

public class Demo {
    public static void main(String[] args) {
        Patient patient1 = new Patient("Андреев", 1991, 34, true);
        Patient patient2 = new Patient("Макаров", 2001, 36, true);
        Patient patient3 = new Patient("Волков", 1981, 30, false);
        Patient patient4 = new Patient("Максимов", 1998, 31, false);
        Patient patient5 = new Patient("Никитин", 2004, 37, true);

        Patient[] patients = {patient1, patient2, patient3, patient4, patient5};

        infOfPatient(patients);
    }

    /**
     * Метод проверки диспансеризации пациентов рожденных до 2000 года
     *
     * @param patients - пациенты
     */
    private static void infOfPatient(Patient[] patients) {
        for (Patient patient : patients) {
            if (patient.isDis() && patient.getNumberOfBorn() < 2000) {
                System.out.println(patient.getSurname() + " - Прошел диспансеризацию \n " + "Информация о пациенте: " + patient.toString());
            }
        }
    }
}
